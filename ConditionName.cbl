       IDENTIFICATION DIVISION.
       PROGRAM-ID. CONDITIONNAMES.
       AUTHOR. Jaruphong Jarumanee.
      *Using Condition names (level 88's) and the EVALUATE
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 CHARIN             PIC X.
          88 VOWEL                 VALUE "a", "e", "i", "o", "u".
          88 CONSONANT             VALUE "b", "c", "d", "e", "f",
                "g"
             ,  "h"
                  , "j" THRU "n", "p" THRU "t", "v" THRU "z".
          88 DIGIT                 VALUE "0" THRU "9".
          88 VALIDCHARACTER        VALUE "a" THRU "z", "0" THRU "9".
       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY
            "Enter lower case character or digit. Invalid char ends."
           ACCEPT CHARIN 
           PERFORM UNTIL NOT VALIDCHARACTER
              EVALUATE TRUE
                 WHEN VOWEL DISPLAY "The latter " CHARIN " is a vowel."
                 WHEN CONSONANT  DISPLAY 
                 "The latter " CHARIN " is a consonant."
                 WHEN DIGIT  DISPLAY CHARIN " is a digit."
               END-EVALUATE
               ACCEPT CHARIN
           End-PERFORM
           STOP RUN.