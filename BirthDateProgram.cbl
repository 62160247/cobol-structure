       IDENTIFICATION DIVISION.
       PROGRAM-ID. BIRTHDATE.
       AUTHOR. Jaruphong Jarumanee.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 BIRTHDATE.
          02 YEAROFBIRTH.
             03 CENTURYOB  PIC 99.
             03 YEAROB     PIC 99.
          02 MOUNTOFBIRTH  PIC 99.
          02 DAYOFBIRTH    PIC 99.
                
       PROCEDURE DIVISION.
           MOVE 19750215 TO BIRTHDATE
           DISPLAY "Month is = " MOUNTOFBIRTH
           DISPLAY "Century of birth is = " CENTURYOB
           DISPLAY "Year of birth is = " YEAROFBIRTH
           DISPLAY DAYOFBIRTH "/" MOUNTOFBIRTH "/" YEAROFBIRTH
           MOVE ZEROS TO YEAROFBIRTH
           DISPLAY "Birth date = " BIRTHDATE.