       IDENTIFICATION DIVISION.
       PROGRAM-ID. CobolGreeting.
       *>Program to display COBOL greetings
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  IterNUM  PIC 9 VALUE 5.

       PROCEDURE DIVISION.
       BeginProgram.
           PERFORM DisplayGreeting IterNUM TIMES.
           STOP RUN.

       DisplayGreeting.
           DISPLAY "Greetings from COBOL".